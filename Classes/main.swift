//
//  main.swift
//  Classes
//
//  Created by Søren Mortensen on 17/06/2017.
//  Copyright © 2017 Søren Mortensen. All rights reserved.
//

import Foundation

// --------------------------------------------------------------------------------------------------------------------
// WHAT ARE CLASSES?                                                                         // MARK: What Are Classes?
// --------------------------------------------------------------------------------------------------------------------

// If you've used records in Pascal, then you'll understand the concept of grouping together variables in order to
// organise them better. In particular, you might want to group together a bunch of variables that all describe a single
// thing or type of thing - like a student or a book.
//
// ```pascal
// type TSubscriber = record
//   firstName, surname: String;
//   address: Array[1..3] of String;
//   phone: String;
//   paidCurrentSubscription: Boolean;
// end;
// ```
//
// Above, the type TSubscriber is a record that contains a bunch of variables all describing a person who belongs to a
// subscription of some sort, like a magazine subscription.
//
// The really convenient thing about records is that they act as a template (or blueprint) for how to create a bunch of
// "instances" of that type. A record type is a type just like a String (for example), and just like you can create
// as many Strings as you like, you can create as many TSubscribers as you like. It looks like this:
//
// ```pascal
// var sub: TSubscriber;
//
// begin
//   sub.firstName := 'Søren';
//   sub.surname := 'Mortensen';
//   sub.address[1] := '123 Road Street';
//   sub.address[2] := 'Town, County';
//   sub.address[3] := 'AB12 3CD';
//   sub.phone := '01234 567890';
//   sub.paidCurrentSubscription := false;
// end.
// ```
//
// Now `sub` is what we call an "instance" of TSubscriber - it's been created in memory and its structure is described
// by the definition of `TSubscriber` above. That means that it contains all the variables (we call them "fields" or
// "properties" when they're in a record) that are listed above, and they all have the same types.
//
// Notice that you access the individual fields on `sub` by using dot syntax - i.e. you write the name of the instance,
// `sub`, followed by a `.`, and then the name of the field. That's not universally used across languages to access
// fields, but it is used in pretty near to all of them. It's the same in Swift, for example.

// However, records are a very simple way of grouping some variables together, and they really don't do much more than
// that. There are some specific things, in fact, that they're missing:
//
// 1. What if you need to perform some kind of operation on the values in the fields on the struct? There isn't really a
//    very good example in this case, but for a more complex record with a lot of fields (and possibly variations in the
//    format of what they could contain), it could get messy trying to write code to do that. More importantly, though,
//    if you needed to do the operation for several different records, or a whole bunch of them in an array, you'd need
//    to put the logic in a function, and call the function on each of the records. That's fine, but before long you'd
//    end up with a lot of different functions that all perform different operations on different types of record, and
//    it would get messy fast. There needs to be a better way of organising them.
//
// 2. What if you have multiple types of subscriber (for example) and they're all slightly different? For example, you
//    might have "Silver Subscriber" and "Gold Subscriber" categories? If the silver subscribers accumulate benefits
//    based on how long they've been a silver subscriber, then you need to keep track of that length of time for each
//    silver subscriber. And what if gold subscribers have that, and also receive a special items on particular dates?
//    You could write additional record definitions like this:
//
// ```pascal
// type TSilverSubscriber = record
//   firstName, surname: String;
//   address: Array[1..3] of String;
//   phone: String;
//   paidCurrentSubscription: Boolean;
//   monthsAsSilver: Integer;
// end;
//
// type TGoldSubscriber = record
//   firstName, surname: String;
//   address: Array[1..3] of String;
//   phone: String;
//   paidCurrentSubscription: Boolean;
//   monthsAsGoldOrSilver: Integer;
//   nextSpecialItem: TDateTime;
// end;
// ```
//
//    Okay, fine... now you have the three different types of subscriber, but what if you need to put TSubscribers,
//    TSilverSubscribers and TGoldSubscribers together in the same list? Or pass any of them into a function without
//    regard for which kind they are? You can't make the type of the array be `Array[0..99] of TSubscriber or
//    TSilverSubscriber or TSilverGoldSubscriber` - that isn't valid Pascal! It has to be one or the other. In the same
//    way, if you're going to pass subscribers into a function, it'd have to look like this:
//
// ```pascal
// procedure MailIssue(subscriber: TSubscriber);
// begin
// ...
// end;
// ```
//
//    Well, if you do that and then try to pass in a TSilverSubscriber... you can't! It's a completely different type,
//    the procedure only takes a TSubscriber! It won't compile.

// So you might be glad to know that there is a solution to all of these problems. The solution is classes!

// In their most basic form, classes look very similar to records. Let me show you one in Swift.

class Subscriber {
    // The first bit looks quite similar to the way a record looks in Pascal. This is a list of variables, each of which
    // is going to be a property of each Subscriber instance.
    var firstName: String
    var surname: String
    var address: [String]
    var phone: String
    var paidCurrentSubscription: Bool
    
    // This is a bit different. This is called an initialiser. Think of it, basically, like a function that sets initial
    // values in all of the properties of a new instance of Subscriber and then returns the new instance.
    init(firstName: String,
         surname: String,                   // When the list of arguments is really long like this (it often is when it
         address: [String],                 // comes to initialisers), we frequently split them over multiple lines.
         phone: String,                     // Don't worry, just look for the curly brace!
         paidCurrentSubscription: Bool) {
        // Inside the initialiser, you use `self` to refer to the new instance that's being created. Imagine that you're
        // "inside" the instance when you're writing code inside the initialiser, and so `self` refers to the instance
        // that you're currently inside.
        self.firstName = firstName
        self.surname = surname
        self.address = address
        self.phone = phone
        self.paidCurrentSubscription = paidCurrentSubscription
    }
}

// The reason we need an initialiser is because the initialiser is what creates instances of the class. So in Swift, in
// order to create an instance of `Subscriber`, we call the initialiser:

let firstSubscriber = Subscriber.init(
    firstName: "Søren",
    surname: "Mortensen",
    address: ["123 Road Street", "Town, County", "AB12 3CD"],
    phone: "01234 567890",
    paidCurrentSubscription: false)

// Notice that we call it just like a function, by listing the arguments in the same order they're declared.

// However, because initialisers are special, there's a slightly shorthand version of this:

let secondSubscriber = Subscriber(
    firstName: "Lakshay",
    surname: "Bhala",
    address: ["321 Boulevard Avenue", "City, State", "EF45 6GH"],
    phone: "09876 54321",
    paidCurrentSubscription: true)

// The only difference is at the end of line 154: the `.init` has been left off, and instead it's just the name of the
// class followed by an opening `(`.

// This is the format that is always used!

// So now you know how to create basic classes, containing some properties and an initialiser, which you can use to
// create instances of that class. Perfect!

// --------------------------------------------------------------------------------------------------------------------
// OBJECTS                                                                                           // MARK: - Objects
// --------------------------------------------------------------------------------------------------------------------

// People often talk about "Object-Oriented Programming" when they talk about classes. So what are objects? We've only
// talked about classes so far.

// It's quite simple, actually. A class is the definition, in code, of the structure of a type, like Subscriber on line
// 113. An object, on the other hand, is an instance of a class!

// So starting on line 113, we have a class called `Subscriber`. On line 143, we create an object, called
// `firstSubscriber`, which is of type `Subscriber`. Then on line 154, we create another object, called
// `secondSubscriber`, which is of type `Subscriber` as well. Both of these are objects, and they come from the same
// class. They contain the same variable structure, but because they're separate bits of memory created independently of
// each other, those variables have completely different values.

// So we can say that every object of type `Subscriber` has a `firstName` property of type `String`, a `surname`
// property of type `String`, and so on. These properties belong to the objects: each object has its own, and they're
// separate. When we get to methods in the next section, those also belong to the objects. However, the initialiser we
// wrote above belongs to the class, not the objects! We can't do this:

//firstSubscriber.init(
//    firstName: "Søren",
//    surname: "Mortensen",
//    address: ["123 Road Street", "Town, County", "AB12 3CD"],
//    phone: "01234 567890",
//    paidCurrentSubscription: false)

// If you uncomment that, it won't compile. That's because
// `init(firstName:surname:address:phone:paidCurrentSubscription:)` belongs to the class, and we tried to call it on an
// object. This is because an initialiser is the code that a class uses to create objects from itself.

// If you're having difficulty wrapping your head around this at first (many people do), try this analogy: the thing we
// call Human (or Homo Sapiens Sapiens) is like a class. It's our concept of what a human is - humans have certain body
// parts, they act a certain way, and they have a whole host of other properties (it's not a coincidence that that's the
// same word we use for variables in a class) that we associate with them. However, Søren and Lakshay are like objects:
// we are particular instances of Human. We both have two eyeballs, hair, opinions on particular foods, and names.
// However, because we're individual, independent humans, we have DIFFERENT eyeballs, hair, opinions on food, and names.

// This analogy is actually quite a good one, because we very frequently use classes to represent real things in the
// world (although that is certainly not all they are used for).

// We can even illustrate this in code, if we wanted to carry the analogy further.

// I'm going to use some advanced stuff in this example, so just ignore things like `enum` if you don't know what they
// are yet. You will before long, I promise! Come back later and look at this and I promise it'll make more sense.
// However, I suspect if you look through it, many things will work exactly the way you assume they work by looking at
// them! Swift is a very intuitive language in many ways.

class Human {
    var name: String
    let leftEyeball: Eyeball
    let rightEyeball: Eyeball
    let hair: Hair
    var likesChocolate: Bool
    
    init(name: String,
         eyeColor: Eyeball.Color,
         hairColor: Hair.Color) {
        self.name = name
        self.leftEyeball = Eyeball(color: eyeColor)
        self.rightEyeball = Eyeball(color: eyeColor)
        self.hair = Hair(color: hairColor)
        self.likesChocolate = true // Everyone likes chocolate
    }
}

class Eyeball {
    let color: Eyeball.Color
    
    init(color: Eyeball.Color) {
        self.color = color
    }
    
    enum Color {
        case brown
        case blue
        case green
        case hazel
    }
}

class Hair {
    let color: Hair.Color
    
    init(color: Hair.Color) {
        self.color = color
    }
    
    enum Color {
        case black
        case brown
        case red
        case blonde
    }
}

// And so here we go: after writing the definitions of those classes, which outline the things we expect every Human to
// have, we can create objects from them - specific instances of `Human`, like you and me.

let søren = Human(name: "Soren", eyeColor: .hazel, hairColor: .brown)
//print("\(søren.name) likes chocolate: \(søren.likesChocolate)")

//let lakshay =
//print("\(lakshay.name) likes chocolate: \(lakshay.likesChocolate)")
// Try it here! ^

// --------------------------------------------------------------------------------------------------------------------
// METHODS                                                                                           // MARK: - Methods
// --------------------------------------------------------------------------------------------------------------------

// So what about those problems I listed at the top (line 57)? Well, let's look at them one at a time. #1 is the problem
// of doing operations on the values inside the class/record.

// Imagine we need to take subscribers' addresses and smash all three strings together into one. Well, we could write a
// function that takes in a subscriber and does it!

func flattenAddress(of subscriber: Subscriber) -> String {
    let street = subscriber.address[0]
    let townCounty = subscriber.address[1]
    let postcode = subscriber.address[2]
    
    let address = "\(street), \(townCounty) \(postcode)"
    return address
}

// So now we can call it like this:

let address1 = flattenAddress(of: firstSubscriber)
let address2 = flattenAddress(of: secondSubscriber)

//print(address1)
//print(address2)

// So that works reasonably well, I suppose, but it's going to get pretty messy if we have lots of operations we need to
// perform on Subscriber objects. There are going to be loads of functions like this around everywhere. Wouldn't it be
// better if we could group them or organise them somehow? The answer is yes, and we can!

// I'm going to write an "extension". This adds code to the existing class above. Just imagine this section of stuff is
// added to the end of the original class definition, as though it had been put in the same set of curly braces
// originally.

extension Subscriber {
    
    // The solution to this problem is that we can put functions inside of classes! Just like the initialiser, they can
    // use `self` to access the properties of the object when they're called. So that means instead of writing a whole
    // bunch of "free functions" outside of the class, we can write what we call "methods" (functions inside a class).
    // And those methods can do all of the same stuff, but in a nicer, more organised way!
    func flattenAddress() -> String {
        // The body of this method is exactly the same as the function above, except we use `self` instead of
        // `subscriber` (because we're no longer passing in the Subscriber object as an argument, but instead it's
        // automatically given to the method as `self`).
        let street = self.address[0]
        let townCounty = self.address[1]
        let postcode = self.address[2]
        
        let address = "\(street), \(townCounty) \(postcode)"
        return address
    }
    
}

// Now we can do the same things as on lines 297 and 298, but by calling the method "on the object" instead!

let address3 = firstSubscriber.flattenAddress()
let address4 = secondSubscriber.flattenAddress()

//print(address3)
//print(address4)

// One of the notable things about this is that it looks like we're "ASKING the class to do something" instead of "doing
// something TO the class". We're asking the Subscriber objects to flatten their addresses and return them to us! That's
// an important thing to notice, because we often talk about classes as intelligent things, as though they know how to
// do things. We even refer to "talking to" and "asking" classes about things.

// Of course, because we can access all of the properties of an object from within its methods, who says we can't access
// its other methods as well?

extension Subscriber {
    
    /// This method first calls the other method, `flattenAddress()`, to get a flat form of the address. Then it prints
    /// it!
    func printAddress() {
        let address = self.flattenAddress()
        print(address)
    }
    
}

//firstSubscriber.printAddress()
//secondSubscriber.printAddress()

// That solves problem #1! We can now perform all sorts of operations on objects by writing methods in the class and
// then calling those methods on the objects. It's very clean and much better organised. There's another reason for why
// we want to do this, but I'll get to that in a later section (hint: it's called encapsulation & access control).

// --------------------------------------------------------------------------------------------------------------------
// INHERITANCE                                                                                   // MARK: - Inheritance
// --------------------------------------------------------------------------------------------------------------------

// But what about problem #2? The problem is, now we want to write a class to represent a Silver Subscriber! So what do
// we do? It needs to have all the same properties as the original Subscriber class, and the same initialiser, and what
// if we want it to have all the same methods as well, like `printAddress()`? Do we write the entire class over again
// and add the Silver Subscriber properties to it, like this?

// ```swift
// class SilverSubscriber {
//     var firstName: String
//     var surname: String
//     var address: [String]
//     var phone: String
//     var paidCurrentSubscription: Bool
//
//     init(firstName: String,
//          surname: String,
//          address: [String],
//          phone: String,
//          paidCurrentSubscription: Bool) {
//         self.firstName = firstName
//         self.surname = surname
//         self.address = address
//         self.phone = phone
//         self.paidCurrentSubscription = paidCurrentSubscription
//     }
//
//     func flattenAddress() -> String {
//         let street = self.address[0]
//         let townCounty = self.address[1]
//         let postcode = self.address[2]
//
//         let address = "\(street), \(townCounty) \(postcode)"
//         return address
//     }
//
//     func printAddress() {
//         let address = self.flattenAddress()
//         print(address)
//     }
// }
// ```

// Definitely not. Look at how much repeated code that is, and then think about what we're going to have to do when we
// want to write the GoldSubscriber class! And what if we want to change the way the `flattenAddress()` method works? We
// have to go and change all three implementations of the method. And if we forget to change one... now everything that
// relies on that method is broken! Sounds like a mess.

// Thankfully, there is a solution. Whenever you want to write a new class that's based on an existing class, you can
// use something called "inheritance." Particularly, we use this when we want to write a class that is a more specific
// version of an existing class. Our SilverSubscribers ARE Subscribers, but they're a more specialised version.

// So what we do is declare the class, `SilverSubscriber`, but we put a little extra bit after its name.

class SilverSubscriber: Subscriber {
    // .................^
    // Think about what a colon (`:`) usually means. When we use it on variables, it means "this is the type of the
    // variable." Well, it means nearly the same thing in this case! A SilverSubscriber "is a type of" Subscriber.
    // Actually, we say that SilverSubscriber "inherits from" Subscriber. So what does that mean?
    
    // Ignore this line until line 452:
//    var monthsAsSilver: Int
}

// It means that, as of right now, the class SilverSubscriber "inherits" EVERYTHING that's inside Subscriber! Even
// though we didn't write them all out, it has the same properties, initialisers, and methods that Subscriber has.

// We use a lot of "family" terms when we're talking about inhertitance (another family term). We say that
// SilverSubscriber is a "child class" of Subscriber, and likewise, Subscriber is a "parent class" of SilverSubscriber.
// Alternatively, the terms that are even more commonly used are "subclass" and "superclass." SilverSubscriber is a
// subclass of Subscriber, and Subscriber is the superclass of SilverSubscriber.

// The upshot of all this is that right now, because SilverSubscriber has inherited everything from Subscriber, we can
// use it in exactly the same way!

let thirdSubscriber = SilverSubscriber(
    firstName: "McFlono",
    surname: "McFloonyloo",
    address: ["84 Circle Lane", "Village, Province", "78IJ 9KL"],
    phone: "01123 581321",
    paidCurrentSubscription: false)

// Plus, `SilverSubscriber` inherits all the methods from `Subscriber`!
//thirdSubscriber.printAddress()

// However, because `SilverSubscriber` is a separate type as well, we can add things to it! Remember the
// `monthsAsSilver` property I mentioned, around line 70? Let's add it. Uncomment line 428 (extensions can't contain
// properties, so I had to add it above, not here).

// Oh no, it broke everything! What on Earth is going on??

// Well, there's really only one problem, and that is that the initialiser SilverSubscriber inherited from Subscriber
// doesn't know how to initialise `monthsAsSilver`, because it doesn't exist on Subscriber, where the initialiser is
// defined! There's a solution, though (big surprise - I always say that!). Comment out lines 442-447.

// Let's write a new initialiser. Unfortunately, we'll have to write a new class, because extensions can't contain
// initialisers either. In a tutorial that's meant to be read by scrolling down the page like this one, that presents
// a bit of a problem. So we'll call the new one `SilverSubscriber2` and use that one from now on.

class SilverSubscriber2: Subscriber {
    
    var monthsAsSilver: Int
    
    // Now some new things are starting to happen. Notice that the declaration of this initialiser looks exactly the
    // same as the old one, but it has the keyword `override` before it! What's all that about?
    
    // Well, the reason that's there is precisely BECAUSE its declaration is the same as the original one (on its
    // superclass). This new initialiser is going to be called INSTEAD of the original one whenever you call it on
    // `SilverSubscriber2` - it replaces it, for the subclass only. This is called "overriding."
    override init(firstName: String,
                  surname: String,
                  address: [String],
                  phone: String,
                  paidCurrentSubscription: Bool) {
        // Remember the reason we've done all this? It's so we can set `monthsAsSilver` to a value when creating
        // instances of `SilverSubscriber2`. So let's do that!
        self.monthsAsSilver = 0
        // We need to do all this because Swift is a very safe language - the compiler will force you to set initial
        // values in all the properties of a class on initialisation, because if those variables don't have values in
        // them later on, you can get all sorts of weird crashes and bugs. Pascal, interestingly, doesn't check ANY of
        // that, so you can have classes with no values in any of their properties if you want! And indeed, it does
        // lead to loads of weird crashes and bugs, particularly for new programmers who aren't used to checking for all
        // this stuff without the compiler forcing them to.
        
        // Now it gets interesting. Remember how we use `self` to access properties of the class we're currently inside,
        // from initialisers and methods? Well, we can use `super` to access the original initialiser on the superclass
        // that we're overriding with this one. That's very helpful, because it means we don't have to duplicate any of
        // the code in the original initialiser. That's not a massive concern in this case, because the original
        // initialiser is very simple, but of course you could do anything in an initialiser, including complicated
        // logic to calculate initial values for properties. We don't want to have to worry about any of that if we just
        // use the original code by calling `super.init`.
        super.init(
            firstName: firstName,
            surname: surname,
            address: address,
            phone: phone,
            paidCurrentSubscription: paidCurrentSubscription)
        
        // In fact, it's more than just convenient; it's required. The compiler checks that too, and you must call
        // `super.init` somewhere in your overridden initialiser.
    }
    
}

// Now we've done that, we can create instances of `SilverSubscriber2`!

let fourthSubscriber = SilverSubscriber2(
    firstName: "McFlono",
    surname: "McFloonyloo",
    address: ["84 Circle Lane", "Village, Province", "78IJ 9KL"],
    phone: "01123 581321",
    paidCurrentSubscription: false)

//print("\(fourthSubscriber.firstName) has been a Silver subscriber for \(fourthSubscriber.monthsAsSilver) months.")

// Now check out something else too:

//print("Is `fourthSubscriber` a `Subscriber`? \(fourthSubscriber is Subscriber)")

// Look at what it prints! Because `SilverSubscriber2` inherits everything from `Subscriber`, it IS a `Subscriber`.
// Remember the function we wrote on line 286? Well, it takes a `Subscriber` as an argument. We can pass in
// fourthSubscriber, which is a `SilverSubscriber2`, to that method, because it's also a `Subscriber`!

//print(flattenAddress(of: fourthSubscriber))

// That makes intuitive sense, though, doesn't it? Think about it with this classic analogy: a rectangle is a type of
// shape, and so is a circle. So you could say "a rectangle is a shape" and "a circle is a shape".

// It works the same way with inheritance:

class Shape { }

class Rectangle: Shape { }
class Circle: Shape { }

let rectangle = Rectangle()
let circle = Circle()

//print("Is `rectangle` a `Rectangle`? \(rectangle is Rectangle)")
//print("Is `rectangle` a `Shape`? \(rectangle is Shape)")
//print("Is `circle` a `Circle`? \(circle is Circle)")
//print("Is `circle` a `Shape`? \(circle is Shape)")

// Just for good measure, I might as well point out that even though `Rectangle` and `Circle` both inherit from `Shape`,
// that doesn't mean there's any relationship between the two!

//print("Is `rectangle` a `Circle`? \(rectangle is Circle)")
//print("Is `circle` a `Rectangle`? \(circle is Rectangle)")

// In addition, of course an instance of `Shape` is neither a `Rectangle` or a `Circle` - it's just a `Shape`.

let shape = Shape()

//print("Is `shape` a `Rectangle`? \(shape is Rectangle)")
//print("Is `shape` a `Circle`? \(shape is Circle)")

// However, unsurprisingly...

//print("Is `shape` a `Shape`? \(shape is Shape)")

// It's just the same with `SilverSubscriber2` and `Subscriber`.

// So why not try to do `GoldSubscriber` as well? I'm going to give you a challenge: uncomment the following class
// definition for `GoldSubscriber`, and fix the error! You know how to do most of it, because it's what we did above. If
// you have questions (particularly about Date, although see if you can figure it out on your own), contact me.

// Hint: `Date` is a class...

//class GoldSubscriber: SilverSubscriber2 {
//
//    var nextSpecialItem: Date
//
//}

// Then you can even add some methods to `GoldSubscriber` if you want to!
